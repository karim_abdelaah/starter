<?php

namespace App\Modules\Admins\Database\Seeders;

use App\Modules\Admins\Models\Admin;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!Admin::count()){
            Admin::create([
                "first_name"         => "Admin",
                "last_name"          => "Admin",
                "email"              => "Super_admin@starter.com",
                "mobile_number"      => "01091811793",
                "password"           => "password",
                "super_admin"        => 1,
                "is_admin"           => 1,
                "is_active"          => 1,
                "profile_picture"    => "",

            ]);

        }
   }
}
