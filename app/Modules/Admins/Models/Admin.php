<?php

namespace App\Modules\Admins\Models;

use App\Modules\BaseApp\Traits\CreatedBy;
use App\Modules\BaseApp\Traits\HasAttach;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laratrust\Traits\LaratrustUserTrait;

class Admin extends Authenticatable
{
    use SoftDeletes, LaratrustUserTrait,
        CreatedBy, HasAttach, Notifiable ;
    protected  $guard="adminguard";


    protected $table = 'admins';

    protected static $attachFields = [
        'profile_picture' => [
            'sizes' => ['small' => 'crop,400x300', 'large' => 'resize,800x600'],
            'path' => 'storage/uploads'
        ],
    ];
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'mobile_number',
        'super_admin',
        'is_admin',
        'password',
        'is_active',
        'profile_picture',
    ];

    public function setPasswordAttribute($value)
    {
        if (trim($value)) {
            $this->attributes['password'] = bcrypt(trim($value));
        }
    }

    public function scopeActive($query)
    {
        return $query->where('is_active', '=', 1);
    }

    public function getNameAttribute($value)
    {
          $name = '';
          if ($this->first_name) {
              $name.= $this->first_name;
          }

          if ($this->last_name) {
              $name.= " " . $this->last_name;
          }

          return $name;
    }

    public function getRoles()
    {
        return Role::get();
    }


    public function scopeNotSuperAdmin($query)
    {
        return $query->where('super_admin', '=', 0);
    }

    public function scopeWithoutLoggedUser($query)
    {
        return $query->where('id', '!=', auth()->id());
    }

    public function getData()
    {
        $query = $this->withoutLoggedUser()
            ->notsuperadmin()
            ->when(request('type'), function ($q) {
                return $q->where('type', request('type'));
            })
            ->when(request('deleted') == 'yes', function ($q) {
                return $q->onlyTrashed();
            });
        return $query;
    }

    public function getLocales()
    {
        $locales = [];

        foreach (config('laravellocalization.supportedLocales') as $key => $value) {
            $locales[$key] = $value['native'];
        }

        return $locales;
    }

    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public function toSearchableArray()
    {
        return $this->toArray();
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
