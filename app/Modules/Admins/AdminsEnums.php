<?php

namespace App\Modules\Admins;

abstract class AdminsEnums
{
    /**
     * List of all Admins's type used in admins table
     */
    public const ADMIN_TYPE = 'admin',
        SUPER_ADMIN_TYPE = 'super_admin';

}
