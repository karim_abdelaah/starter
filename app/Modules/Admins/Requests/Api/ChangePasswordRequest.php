<?php

namespace App\Modules\Users\Requests\Api;

use App\Modules\BaseApp\Requests\BaseApiTokenDataRequest;

class ChangePasswordRequest extends BaseApiTokenDataRequest
{
    public function rules()
    {
        return [
            'old_password' => 'required',
            'password' => ['required', 'min:8', 'regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%]).*$/', 'confirmed']
        ];
    }
}
