<?php

namespace App\Modules\Users\Requests\Api;

use App\Modules\BaseApp\Requests\BaseApiParserRequest;
use App\Modules\Users\AdminsEnums;
use Illuminate\Validation\Rule;

class MobileResetPasswordRequest extends BaseApiParserRequest
{
    public function rules()
    {
        return [
            "attributes.old-password" =>"nullable",
            "attributes.password"             =>  [
                "min:6",
                "regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%]).*$/",
                "confirmed"
            ],
        ];
    }
}
