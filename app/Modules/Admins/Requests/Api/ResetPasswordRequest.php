<?php

namespace App\Modules\Users\Requests\Api;

use App\Modules\BaseApp\Requests\BaseApiTokenDataRequest;

class ResetPasswordRequest extends BaseApiTokenDataRequest
{
    public function rules()
    {
        return [
            "password"  =>  [
                "required",
                'regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%]).*$/',
                "confirmed",
            ]
        ];
    }

}
