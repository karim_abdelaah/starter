<?php

namespace App\Modules\BaseApp\Requests\Admin;

use App\Modules\BaseApp\Requests\BaseAppRequest;
use Illuminate\Validation\Rule;

class AdminLoginRequest extends BaseAppRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }
}
