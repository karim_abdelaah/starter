<?php

namespace App\Modules\BaseApp\Controllers\Admin;

use App\Http\Controllers\Controller;

class BaseAdminController extends Controller
{

    private $view;
    private $module;
    private $title;
    public function __construct()
    {
        $this->view = 'BaseApp::Admin';
        $this->module = 'Home';
        $this->title = trans('app.Home');
    }

    public function index()
    {
        $data['module'] = $this->module;
        $data['page_title'] =$this->title;
        return view($this->view.'.index' , $data);
    }
}
