<?php
namespace App\Modules\BaseApp\Controllers\Api;

use App\Modules\BaseApp\Traits\ApiResponser;
use App\Http\Controllers\Controller;

class BaseApiController extends Controller
{
    use ApiResponser;
}
