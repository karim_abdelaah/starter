<?php

use App\Modules\BaseApp\Controllers\Admin\AuthController;

Route::group(['prefix' => LaravelLocalization::setLocale().'/admin/'],
 function () {
     Route::get('/login'           , [ AuthController::class,'getLogin'])->name('admin.getloginpage');
     Route::post('/login'          , [ AuthController::class,'postLogin'])->name('admin.login');
     Route::get('/forget'          , [ AuthController::class,'getForgotPassword'])->name('admin.getforgetpage');
     Route::post('/forget'         , [ AuthController::class,'postForgotPassword'])->name('admin.forget');
     Route::get('/logout'          , [ AuthController::class,'getLogout'])->name('admin.logout');

     Route::group([
    'middleware' => [ 'localeSessionRedirect', 'localizationRedirect',
        'localeViewPath','auth:adminguard']
], function () {
    Route::get('/','BaseAdminController@index')->name('admin.dashboard');
});
});
