<li class="nav-item {{(request()->getRequestUri() == "/".lang())?"active":""}}">
    <a class="nav-link" href="{{ route('admin.dashboard') }}">
        <i class="icon ion-ios-pie-outline"></i>
        <span>{{trans('navigation.Dashboard')}}</span>
    </a>
</li>

{{--@if(can('create-pages') || can('view-users') || can('view-countries') || can('view-news') || can('view-roles') || can('view-options'))--}}

    <li class="nav-item with-sub mega-dropdown {{(request()->is('*/pages*' , '*/news*' , '*/countries*' , '*/users*','*/roles*'.'*/options*'))?"active":""}}">
        <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="true">
            <i class="icon ion-ios-analytics-outline"></i>
            <span> {{trans('navigation.Entities Management')}}</span>
        </a>
        <div class="sub-item">
            <div class="row">
                <div class="col-lg mg-t-30 mg-lg-t-0">
                    <div class="row">
                        <div class="col">
                            <label class="section-label">{{trans('navigation.Users')}}</label>
                            <ul>
                               <li class="{{(request()->is('*/users*'))?"active":""}}">
                                    <a href="/users">{{trans('navigation.All Users')}}</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg mg-t-30 mg-lg-t-0">
                    <div class="row">
                        <div class="col">
                            <label class="section-label">{{trans('navigation.Dynamic Settings')}}</label>
{{--                            <ul>--}}
{{--                                <li class="{{(request()->is('*/pages*'))?"active":""}}">--}}
{{--                                    <a href="/calculator/edit/1">{{trans('navigation.Calculator')}}</a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
                        </div>
                    </div>
                </div>
                <div class="col-lg mg-t-30 mg-lg-t-0">
                    <div class="row">
                        <div class="col">
                            <label class="section-label">{{trans('navigation.News')}}</label>
{{--                            <ul>--}}
{{--                                <li class="{{(request()->is('*/categories*'))?"active":""}}">--}}
{{--                                    <a href="/categories">{{trans('navigation.Categories')}}</a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
                        </div>
                    </div>
                </div>
                <div class="col-lg mg-t-30 mg-lg-t-0">
                    <div class="row">
                        <div class="col">
                            <label class="section-label">{{trans('navigation.Logs & Activities')}}</label>
{{--                            <ul>--}}
{{--                                <li class="{{(request()->is('*/activities*'))?"active":""}}">--}}
{{--                                    <a href="/activities">{{trans('navigation.Activities')}}</a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
                        </div>
                    </div>
                </div>
                <div class="col-lg mg-t-30 mg-lg-t-0">
                    <div class="row">
                        <div class="col">
                            <label class="section-label">{{trans('navigation.Projects')}}</label>
{{--                            <ul>--}}
{{--                                <li class="{{(request()->is('*/areas*'))?"active":""}}">--}}
{{--                                    <a href="/areas">{{trans('navigation.Areas')}}</a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
                        </div>
                    </div>
                </div>

                <div class="col-lg mg-t-30 mg-lg-t-0">
                    <div class="row">
                        <div class="col">
                            <label class="section-label">{{trans('navigation.About Us')}}</label>
{{--                            <ul>--}}
{{--                                <li class="{{(request()->is('*/vision-mission*'))?"active":""}}">--}}
{{--                                    <a href="/vision-mission">{{trans('navigation.Vision And Mission')}}</a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </li>

{{--@endif--}}
{{-- only super admin can access configuration settings --}}
{{--@if(@auth()->user()->type == App\Modules\Users\AdminsEnums::SUPER_ADMIN_TYPE)--}}


    <li class="nav-item with-sub settings {{(request()->is( '*/translator*' , '*/configs*' ))?"active":""}}">
        <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="true">
            <i class="icon ion-ios-gear-outline"></i>
            <span>{{trans('navigation.Settings')}}</span>
        </a>
        <div class="sub-item">
{{--            <ul>--}}
{{--                <li class="{{(request()->is('*/translator*'))?"active":""}}">--}}
{{--                    <a href="{{route('translator.getIndex')}}">{{trans('navigation.translator')}}</a>--}}
{{--                </li>--}}

{{--                <li class="{{(request()->is('*/configs*'))?"active":""}}">--}}
{{--                    <a href="/configs/edit">{{trans('navigation.Configurations')}}</a>--}}
{{--                </li>--}}

{{--            </ul>--}}
        </div><!-- dropdown-menu -->
    </li>
{{--@endif--}}
