<div class="slim-footer">
    <div class="container">
        <p>{{ trans('app.Copyright') }} {{ date('Y') }} &copy; {{ trans('app.All Rights Reserved') }}. {{ env('APP_NAME') }}</p>
        <p>{{ trans('app.Developed by') }}: {{ trans('app.Developed by String') }} </p>
    </div><!-- container -->
</div>
