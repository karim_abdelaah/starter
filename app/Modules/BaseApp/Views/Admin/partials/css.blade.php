<!-- <link rel="preload" as="font" href="fonts/ionicons.ttf" crossorigin="anonymous"> -->
<!-- Vendors CSS -->
<link rel="stylesheet" href="Admin/css/vendors.css?id=1">
<!-- APP CSS -->
<link rel="stylesheet" href="Admin/css/app.css?id=1">

@if(lang() == 'ar')
    <style>
        table.dataTable thead th.sorting::after, table.dataTable thead th.sorting_asc::after, table.dataTable thead th.sorting_desc::after, table.dataTable thead td.sorting::after, table.dataTable thead td.sorting_asc::after, table.dataTable thead td.sorting_desc::after {
            right: unset;
            left: 8px;
        }
        table.dataTable thead th.sorting::before, table.dataTable thead th.sorting_asc::before, table.dataTable thead th.sorting_desc::before, table.dataTable thead td.sorting::before, table.dataTable thead td.sorting_asc::before, table.dataTable thead td.sorting_desc::before {
            right: unset;
            left: 8px;
        }
    </style>
@endif
