<!DOCTYPE html>
<html lang="{{ LaravelLocalization::getCurrentLocale() }}" dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}">

    <head>
        @include('BaseApp::Admin.partials.meta')
        @include('BaseApp::Admin.partials.css')
        @stack('css')
    </head>

    <body>
        <div class="d-md-flex flex-row-reverse">
            @include('BaseApp::Admin.partials.flash_messages')
            @yield('content')
        </div>
        <!-- d-flex -->
        <!-- signin-wrapper -->
        @include('BaseApp::Admin.partials.js') @stack('js')
    </body>

</html>
