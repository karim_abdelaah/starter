<!DOCTYPE html>
<html lang="{{ LaravelLocalization::getCurrentLocale() }}" dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}">

    <head>
        @include('BaseApp::Admin.partials.meta')
        @include('BaseApp::Admin.partials.css')
        @stack('css')
    </head>

    <body>
        @include('BaseApp::Admin.partials.header')
        <!-- slim-header -->
        @include('BaseApp::Admin.partials.navigation')
        <!-- slim-navbar -->
        <div class="slim-mainpanel">
            <div class="container" id="app">
                @include('BaseApp::Admin.partials.breadcrumb')
                @include('BaseApp::Admin.partials.flash_messages')
                <!-- section-wrapper -->
                @yield('content')
                <!-- section-wrapper -->
            </div>
            <!-- container -->
        </div>
        <!-- slim-mainpanel -->
        <!-- slim-footer -->
        @include('BaseApp::Admin.partials.footer')
        @include('BaseApp::Admin.partials.js')
        @stack('js')
    </body>

</html>
