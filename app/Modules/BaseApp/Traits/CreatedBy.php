<?php

namespace App\Modules\BaseApp\Traits;

use App\Modules\Users\Admin;
use Illuminate\Support\Facades\DB;

trait CreatedBy
{
    public static function bootCreatedBy()
    {
        static::saved(function ($model) {
            if (!$model->created_by && $model->table != null) {
                if ($user = auth()->user()) {
                    DB::table($model->table)->where('id', $model->id)->update(['created_by' => (@$user->id)? : null]);
                }
            }
        });
    }

    public function creator()
    {
        return $this->belongsTo(Admin::class, 'created_by')->withTrashed()->withDefault();
    }
}
