@foreach($rows['en'] as $key=>$value)
    @if($key)
    <h5 class="edit-trans-title">{{$key}}</h5>
        @foreach(config("translatable.locales") as $lang)
            @if(isset($rows[$lang][$key]) && !is_array($rows[$lang][$key]))
                @include('BaseApp::form.input',['name'=>$lang.'['.$key.']','value'=>$rows[$lang][$key],'type'=>'text','attributes'=>['class'=>'form-control','label'=>ucfirst($lang),'translate'=>'edit-trans']])
            @endif
        @endforeach
    @endif
@endforeach
