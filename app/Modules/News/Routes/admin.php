<?php

Route::group([
    'prefix' =>'admin/'. LaravelLocalization::setLocale(),
    'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath','auth:adminguard']
], function () {
    Route::get('/testR',function (){
        return lang();
    });
});
