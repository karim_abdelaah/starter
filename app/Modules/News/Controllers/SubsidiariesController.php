<?php

namespace App\Modules\AboutUs\Controllers;

use App\Modules\BaseApp\Controllers\BaseController;
use App\Modules\AboutUs\Models\Subsidiaries;
use App\Modules\AboutUs\Requests\SubsidiariesRequest;
use App\Modules\AbouUs\Models\SubsidiariesImage;
use Illuminate\Support\Str;

class SubsidiariesController extends BaseController {

    public $model;
    public $views;
    public $module;

    public function __construct(Subsidiaries $model) {
        $this->module = 'subsidiaries';
        $this->views = 'News';
        $this->title = trans('app.Subsidiaries');
        $this->model = $model;
        $this->rules = $model->rules;
    }

    public function getIndex() {
        authorize('view-' . $this->module);
        $data['module'] = $this->module;
        $data['page_title'] = trans('app.List') . " " . $this->title;
        $data['rows'] = $this->model->getData()->get();
        return view($this->views . '::subsidiaries.index', $data);
    }

    public function getCreate() {
        authorize('create-' . $this->module);
        $data['module'] = $this->module;
        $data['views'] = $this->views;
        $data['page_title'] = trans('app.Create') . " " . $this->title;
        $data['breadcrumb'] = [$this->title => $this->module];
        $data['row'] = $this->model;
        $data['row']->is_active = 1;
        return view($this->views . '::subsidiaries.create', $data);
    }

    public function postCreate(SubsidiariesRequest $request) {
        authorize('create-' . $this->module);
        if ($row = $this->model->create($request->all())) {
            flash()->success(trans('app.Created successfully'));
            return redirect('/' . $this->module);
        }
        flash()->error(trans('app.failed to save'));
        return back();
    }

    public function getEdit() {
        authorize('edit-' . $this->module);
        $data['module'] = $this->module;
        $data['views'] = $this->views;
        $data['page_title'] = trans('app.Edit') . " " . $this->title;
        $data['breadcrumb'] = [$this->title => $this->module];
        $data['row'] = $this->model->first();
        return view($this->views . '::subsidiaries.edit', $data);
    }

    public function postEdit(SubsidiariesRequest $request) {
        authorize('edit-' . $this->module);
        $row = $this->model->first();

        $images = [];
        $uploadPath = public_path("uploads/".$this->module);

        if ($request->hasFile('images')) {
            foreach ($request->images as $image ) {
                $fileName = strtolower(Str::random(10)) . time() . '.' . $image->getClientOriginalExtension();
                $image->move($uploadPath, $fileName);
                $images[] = ["subsidiaries_id"=>$row->id,"image"=>$fileName];
            }
        }
        SubsidiariesImage::insert($images);

        if ($row->update($request->all())) {
            flash(trans('app.Update successfully'))->success();
            return back();
        }
    }

    public function getView() {
        authorize('view-' . $this->module);
        $data['module'] = $this->module;
        $data['page_title'] = trans('app.View') . " " . $this->title;
        $data['breadcrumb'] = [$this->title => $this->module];
        $data['row'] = $this->model->first();
        return view($this->views . '::subsidiaries.view', $data);
    }

    public function deleteImage($id) {
        authorize('delete-' . $this->module);
        $row = SubsidiariesImage::where("id",$id)->first();
        if ($row->delete()) {
            $this->deletLocalImage(public_path('uploads/'.$this->module),$row->image);
        }
        flash()->success(trans('app.Deleted Successfully'));
        return redirect()->back();
    }

    public function deletLocalImage($path,$fileName)
    {
        $file = $path.'/'.$fileName;
        if (file_exists($file)) {
            \File::delete($file);
        }
    }

    // public function getExport() {
    //     authorize('view-' . $this->module);
    //     $rows = $this->model->getData()->get();
    //     if ($rows->isEmpty()) {
    //         flash()->error(trans('app.There is no results to export'));
    //         return back();
    //     }
    //     $this->model->export($rows,$this->module);
    // }

}
