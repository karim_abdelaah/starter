<?php

namespace App\Modules\AboutUs\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\AboutUs\Models\Category;
use App\Modules\AboutUs\Models\VisionAndMissions;
use App\Modules\AboutUs\Models\VissionAndMission;
use App\Modules\AboutUs\Requests\VisionMissionRequest;
use App\Modules\AboutUs\Requests\VissionAndMissionRequest;

class VisionAndMissionController extends Controller {

    public $model;
    public $views;
    public $module;

    public function __construct(VisionAndMissions $model) {
        $this->module = 'vision-mission';
        $this->views = 'News';
        $this->title = trans('app.Vision And Missions');
        $this->model = $model;
        $this->rules = $model->rules;
    }

    public function getIndex() {
        authorize('view-' . $this->module);
        $data['module'] = $this->module;
        $data['page_title'] = trans('app.List') . " " . $this->title;
        $data['rows'] = $this->model->getData()->get();
        return view($this->views . '::vision_mission.index', $data);
    }

    // public function getCreate() {
    //     authorize('create-' . $this->module);
    //     $data['module'] = $this->module;
    //     $data['views'] = $this->views;
    //     $data['page_title'] = trans('app.Create') . " " . $this->title;
    //     $data['breadcrumb'] = [$this->title => $this->module];
    //     $data['row'] = $this->model;
    //     $data['row']->is_active = 1;

    //     return view($this->views . '::vision_mission.create', $data);
    // }

    // public function postCreate(VisionMissionRequest $request) {
    //     authorize('create-' . $this->module);
    //     if ($row = $this->model->create($request->all())) {
    //         flash()->success(trans('app.Created successfully'));
    //         return redirect('/' . $this->module);
    //     }
    //     flash()->error(trans('app.failed to save'));
    //     return back();
    // }

    public function getEdit() {
        authorize('edit-' . $this->module);
        $data['module'] = $this->module;
        $data['views'] = $this->views;
        $data['page_title'] = trans('app.Edit') . " " . $this->title;
        $data['breadcrumb'] = [$this->title => $this->module];
        $data['row'] = $this->model->first();
        return view($this->views . '::vision_mission.edit', $data);
    }

    public function postEdit(VisionMissionRequest $request) {
        authorize('edit-' . $this->module);
        $row = $this->model->first();
        if ($row->update($request->all())) {
            flash(trans('app.Update successfully'))->success();
            return back();
        }
    }

    public function getView() {
        authorize('view-' . $this->module);
        $data['module'] = $this->module;
        $data['page_title'] = trans('app.View') . " " . $this->title;
        $data['breadcrumb'] = [$this->title => $this->module];
        $data['row'] = $this->model->first();
        return view($this->views . '::vision_mission.view', $data);
    }

    // public function getDelete($id) {
    //     authorize('delete-' . $this->module);
    //     $row = $this->model->findOrFail($id);
    //     $row->delete();
    //     flash()->success(trans('app.Deleted Successfully'));
    //     return redirect( '/' . $this->module);
    // }

}
