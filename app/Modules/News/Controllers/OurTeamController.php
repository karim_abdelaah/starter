<?php

namespace App\Modules\AboutUs\Controllers;

use App\Modules\BaseApp\Controllers\BaseController;
use App\Modules\AboutUs\Models\OurTeam;
use App\Modules\AboutUs\Requests\OurTeamRequest;
use App\Modules\AboutUs\Requests\OurTeamUpdateRequest;

class OurTeamController extends BaseController {

    public $model;
    public $views;
    public $module;

    public function __construct(OurTeam $model) {
        $this->module = 'our-team';
        $this->views = 'News';
        $this->title = trans('app.OurTeam');
        $this->model = $model;
        $this->rules = $model->rules;
    }

    public function getIndex() {
        authorize('view-' . $this->module);
        $data['module'] = $this->module;
        $data['page_title'] = trans('app.List') . " " . $this->title;
        $data['rows'] = $this->model->getData()->orderBy("id","DESC")->get();
        return view($this->views . '::our_team.index', $data);
    }

    public function getCreate() {
        authorize('create-' . $this->module);
        $data['module'] = $this->module;
        $data['views'] = $this->views;
        $data['page_title'] = trans('app.Create') . " " . $this->title;
        $data['breadcrumb'] = [$this->title => $this->module];
        $data['row'] = $this->model;
        $data['row']->is_active = 1;
        return view($this->views . '::our_team.create', $data);
    }

    public function postCreate(OurTeamRequest $request) {
        authorize('create-' . $this->module);
        if ($row = $this->model->create($request->all())) {
            flash()->success(trans('app.Created successfully'));
            return redirect('/' . $this->module);
        }
        flash()->error(trans('app.failed to save'));
        return back();
    }

    public function getEdit($id) {
        authorize('edit-' . $this->module);
        $data['module'] = $this->module;
        $data['views'] = $this->views;
        $data['page_title'] = trans('app.Edit') . " " . $this->title;
        $data['breadcrumb'] = [$this->title => $this->module];
        $data['row'] = $this->model->findOrFail($id);
        return view($this->views . '::our_team.edit', $data);
    }

    public function postEdit(OurTeamUpdateRequest $request , $id) {
        authorize('edit-' . $this->module);
        $row = $this->model->findOrFail($id);
        if ($row->update($request->all())) {
            flash(trans('app.Update successfully'))->success();
            return back();
        }
    }

    public function getView($id) {
        authorize('view-' . $this->module);
        $data['module'] = $this->module;
        $data['page_title'] = trans('app.View') . " " . $this->title;
        $data['breadcrumb'] = [$this->title => $this->module];
        $data['row'] = $this->model->findOrFail($id);
        return view($this->views . '::our_team.view', $data);
    }

    public function getDelete($id) {
        authorize('delete-' . $this->module);
        $row = $this->model->findOrFail($id);
        $row->delete();
        flash()->success(trans('app.Deleted Successfully'));
        return redirect( '/' . $this->module);
    }

    public function getExport() {
        authorize('view-' . $this->module);
        $rows = $this->model->getData()->get();
        if ($rows->isEmpty()) {
            flash()->error(trans('app.There is no results to export'));
            return back();
        }
        $this->model->export($rows,$this->module);
    }

}
