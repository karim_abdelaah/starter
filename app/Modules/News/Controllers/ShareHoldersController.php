<?php

namespace App\Modules\AboutUs\Controllers;

use App\Modules\BaseApp\Controllers\BaseController;
use App\Modules\AboutUs\Models\ShareHolder;
use App\Modules\AboutUs\Requests\ShareHolderRequest;
use App\Modules\AbouUs\Models\ShareHolderImage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class ShareHoldersController extends BaseController {

    public $model;
    public $views;
    public $module;

    public function __construct(ShareHolder $model) {
        $this->module = 'shareholders';
        $this->views = 'News';
        $this->title = trans('app.ShareHolder');
        $this->model = $model;
        $this->rules = $model->rules;
    }

    public function getIndex() {
        authorize('view-' . $this->module);
        $data['module'] = $this->module;
        $data['page_title'] = trans('app.List') . " " . $this->title;
        $data['rows'] = $this->model->getData()->get();
        return view($this->views . '::shareholders.index', $data);
    }

    // public function getCreate() {
    //     authorize('create-' . $this->module);
    //     $data['module'] = $this->module;
    //     $data['views'] = $this->views;
    //     $data['page_title'] = trans('app.Create') . " " . $this->title;
    //     $data['breadcrumb'] = [$this->title => $this->module];
    //     $data['row'] = $this->model;
    //     $data['row']->is_active = 1;
    //     return view($this->views . '::shareholders.create', $data);
    // }

    // public function postCreate(ShareHolderRequest $request) {
    //     authorize('create-' . $this->module);
    //     if ($row = $this->model->create($request->all())) {
    //         flash()->success(trans('app.Created successfully'));
    //         return redirect('/' . $this->module);
    //     }
    //     flash()->error(trans('app.failed to save'));
    //     return back();
    // }

    public function getEdit() {
        authorize('edit-' . $this->module);
        $data['module'] = $this->module;
        $data['views'] = $this->views;
        $data['page_title'] = trans('app.Edit') . " " . $this->title;
        $data['breadcrumb'] = [$this->title => $this->module];
        $data['row'] = $this->model->with('images')->first();
        return view($this->views . '::shareholders.edit', $data);
    }

    public function postEdit(ShareHolderRequest $request) {
        authorize('edit-' . $this->module);
        $row = $this->model->first();

        $images = [];
//        $uploadPath = storage_path("uploads/".$this->module);

        if ($request->hasFile('images')) {
            foreach ($request->images as $image ) {

                $fileName = strtolower(Str::random(10)) . time() . '.' . $image->getClientOriginalExtension();
                $imageSizes = [
                    'large'  => 'resize,500x500',
                    'small' => 'resize,200x200'
                ];
                foreach ($imageSizes as $key => $value) {
                    $value = explode(',', $value);
                    $type = $value[0];
                    $dimensions = explode('x', $value[1]);
                    $thumbPath = storage_path('app/public/uploads/' . $key . '/' . $fileName);
                    $image =Image::make($image);
                    if ($type == 'crop') {
                        $image->fit($dimensions[0], $dimensions[1]);
                    } else {
                        $image->resize($dimensions[0], $dimensions[1], function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    }
                    $image->save($thumbPath);
                }











//                $image->move($uploadPath, $fileName);
                $images[] = ["share_holders_id"=>$row->id,"image"=>$fileName];
            }
        }
        ShareHolderImage::insert($images);

        if ($row->update($request->all())) {
            flash(trans('app.Update successfully'))->success();
            return back();
        }
    }

    public function getView() {
        authorize('view-' . $this->module);
        $data['module'] = $this->module;
        $data['page_title'] = trans('app.View') . " " . $this->title;
        $data['breadcrumb'] = [$this->title => $this->module];
        $data['row'] = $this->model->first();
        return view($this->views . '::shareholders.view', $data);
    }

    public function deleteImage($id) {
        authorize('delete-' . $this->module);
        $row = ShareHolderImage::where("id",$id)->first();
        if ($row->delete()) {
            $this->deletLocalImage(public_path('uploads/'.$this->module),$row->image);
        }
        flash()->success(trans('app.Deleted Successfully'));
        return redirect()->back();
    }

    public function deletLocalImage($path,$fileName)
    {
        $file = $path.'/'.$fileName;
        if (file_exists($file)) {
            \File::delete($file);
        }
    }

    // public function getExport() {
    //     authorize('view-' . $this->module);
    //     $rows = $this->model->getData()->get();
    //     if ($rows->isEmpty()) {
    //         flash()->error(trans('app.There is no results to export'));
    //         return back();
    //     }
    //     $this->model->export($rows,$this->module);
    // }

}
