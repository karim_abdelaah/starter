<?php

namespace App\Modules\Configs\Transformers;

use App\Modules\Configs\Configs;
use League\Fractal\TransformerAbstract;

class ContactInfoMobileTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
    ];
    protected $availableIncludes = [
    ];

    /**
     * @param Configs $config
     * @return array
     */
    public function transform($configs)
    {
        $transfromedData =  [
            'id' => 'contactInfo',
        ];
        if($configs){
            foreach ($configs as $key => $value){
                $transfromedData[$key] = $value;
            }
        }

        return $transfromedData;
    }
    public static function originalAttribute($index)
    {
        $attributes = [
            'id' => 'id',
            'label' => 'label',
            "value"=>"value"
        ];
        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'id',
            'label' => 'label',
            "value" =>  "value"
        ];
        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

}
