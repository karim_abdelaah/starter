<?php

namespace App\Modules\Configs\Transformers;

use App\Modules\Configs\Configs;
use League\Fractal\TransformerAbstract;

class ContactInfoTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
    ];
    protected $availableIncludes = [
    ];

    /**
     * @param Configs $config
     * @return array
     */
    public function transform(Configs $config)
    {
        $transfromedData =  [
            'id' => (int) $config->id,
            'key' => (string) $config->field,
            'label' => (string) $config->label,
            "value" =>  (string) $config->value,
        ];

        return $transfromedData;
    }
    public static function originalAttribute($index)
    {
        $attributes = [
            'id' => 'id',
            'label' => 'label',
            "value"=>"value"
        ];
        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'id',
            'label' => 'label',
            "value" =>  "value"
        ];
        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

}
