<?php


namespace App\Modules\Configs;
use App\Modules\BaseApp\BaseModel;

class ConfigsTranslation extends BaseModel
{
    public $timestamps = false;
    protected $table = 'config_translations';
    protected $fillable = [
        'label',
        'value'
    ];
}
