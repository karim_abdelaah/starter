{{-- {{$rows}} --}}
@foreach($rows as $key=>$value)
<h5>{{$key}}</h5>
    @foreach($value as $row)
            @if($row->field_type=='file')
                @include('BaseApp::form.file',['name'=>$row->field.":ar",'attributes'=>['class'=>'form-control custom-file-input','label'=>$row->translateOrDefault("ar")->label,'value'=>$row->translateOrDefault("ar")->value]])
            @elseif(in_array($row->field , $to_be_translated))
                @foreach(langs() as $lang)
                    @include('BaseApp::form.input',
                            [
                                'type'=>$row->field_type,
                                'name'=>$row->field.":".$lang,
                                'value'=>$row->translateOrDefault($lang)->value,
                                'attributes'=>
                                    ['class'=>
                                        'form-control '.$row->translateOrDefault($lang)->field_class,
                                        'label'=>$row->translateOrDefault($lang)->label." ".$lang,
                                        $row->translateOrDefault($lang)->field
                                    ]
                            ])
                @endforeach
            @else
                @include('BaseApp::form.input',['type'=>$row->field_type,'name'=>$row->field.":ar",'value'=>$row->translateOrDefault("ar")->value,'attributes'=>['class'=>'form-control '.$row->translateOrDefault("ar")->field_class,'label'=>$row->translateOrDefault("ar")->label,$row->translateOrDefault("ar")->field]])
            @endif
    @endforeach
@endforeach
