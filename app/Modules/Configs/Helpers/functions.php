<?php
if (! function_exists('insertDefaultConfigs')) {
    function insertDefaultConfigs()
    {
        \Cache::forget('configs');
        @copy(public_path() . '/img/logo.png', storage_path() . '/app/public/uploads/small/logo.png');
        @copy(public_path() . '/img/logo.png', storage_path() . '/app/public/uploads/large/logo.png');
        @copy(public_path() . '/img/9c298c3.png', storage_path() . '/app/public/uploads/small/9c298c3.png');
        @copy(public_path() . '/img/9c298c3.png', storage_path() . '/app/public/uploads/large/9c298c3.png');
        $rows = [];
        //////////// $appName
        $appName = env('APP_NAME');
        $row = [
            'field_type' => 'text',
            'field_class' => '',
            'type' => 'Basic Information',
            'field' => 'application_name',
            'label:en' => 'Application Name',
            'label:ar' => 'Application Name',
            'value:en' => $appName,
            'value:ar' => $appName,
            'created_by' => 2,
        ];
        $rows[] = $row;
        //////////// $welcome
        $welcome = 'Welcome to our application';
        $row = [
            'field_type' => 'text',
            'field_class' => '',
            'type' => 'Basic Information',
            'field' => 'welcome',
            'label:en' => 'Welcome Message',
            'label:ar' => 'Welcome Message',
            'value:en' => $welcome,
            'value:ar' => $welcome,
            'created_by' => 2,
        ];
        $rows[] = $row;
        ///////////////// Logo
        $rows[] = [
            'field_type' => 'file',
            'field_class' => 'custom-file-input',
            'type' => 'Basic Information',
            'field' => 'logo',
            'label:en' => 'Logo',
            'label:ar' => 'Logo',
            'value:en' => 'logo.png',
            'value:ar' => 'logo.png',
            'created_by' => 2,
        ];
        ///////////////// Email
        $rows[] = [
            'field_type' => 'text',
            'field_class' => '',
            'type' => 'Contact Information',
            'field' => 'email',
            'label:en' => 'Email',
            'label:ar' => 'Email',
            'value:en' => env('CONTACT_EMAIL', 'contact@bcadminstarter.com'),
            'value:ar' => env('CONTACT_EMAIL', 'contact@bcadminstarter.com'),
            'created_by' => 2,
        ];
        ///////////////// Phone
        $rows[] = [
            'field_type' => 'text',
            'field_class' => '',
            'type' => 'Contact Information',
            'field' => 'landline',
            'label:en' => 'Landline',
            'label:ar' => 'Landline',
            'value:en' => '12345678',
            'value:ar' => '12345678',

            'created_by' => 2,
        ];
        ///////////////// Mobile
        $rows[] = [
            'field_type' => 'text',
            'field_class' => '',
            'type' => 'Contact Information',
            'field' => 'hotline',
            'label:en' => 'Hotline',
            'label:ar' => 'Hotline',
            'value:en' => '12345678',
            'value:ar' => '12345678',
            'created_by' => 2,
        ];
        ///////////////// longitude
        $rows[] = [
            'field_type' => 'text',
            'field_class' => '',
            'type' => 'Contact Information',
            'field' => 'longitude',
            'label:en' => 'Locaion (longitude)',
            'label:ar' => 'Locaion (longitude)',
            'value:en' => '31.324104799999986',
            'value:ar' => '31.324104799999986',
            'created_by' => 2,
        ];
        ///////////////// latitude
        $rows[] = [
            'field_type' => 'text',
            'field_class' => '',
            'type' => 'Contact Information',
            'field' => 'latitude',
            'label:en' => 'Locaion (latitude)',
            'label:ar' => 'Locaion (latitude)',
            'value:en' => '30.0685382',
            'value:ar' => '30.0685382',
            'created_by' => 2,
        ];

        ///////////////// latitude
        $rows[] = [
            'field_type' => 'textarea',
            'field_class' => '',
            'type' => 'Contact Information',
            'field' => 'contact_title',
            'label:en' => 'contact info title',
            'label:ar' => 'contact info title',
            'value:en' => 'We’d love to hear from you!',
            'value:ar' => 'We’d love to hear from you!',
            'created_by' => 2,
        ];

        $rows[] = [
            'field_type' => 'textarea',
            'field_class' => '',
            'type' => 'Contact Information',
            'field' => 'contact_description',
            'label:en' => 'contact info desccription',
            'label:ar' => 'contact info desccription',
            'value:en' => 'Don’t hesitate to get in touch with any inquiries; shortly one of our friendly staff members will assist you.',
            'value:ar' => 'Don’t hesitate to get in touch with any inquiries; shortly one of our friendly staff members will assist you.',
            'created_by' => 2,
        ];

        $rows[] = [
            'field_type' => 'text',
            'field_class' => '',
            'type' => 'Contact Information',
            'field' => 'head_office',
            'label:en' => 'Head Office',
            'label:ar' => 'Head Office',
            'value:en' => 'Cairo Center, 12th Floor, 106, ELKasr Al Ainy,Garden City, Cairo, Egypt',
            'value:ar' => 'Cairo Center, 12th Floor, 106, ELKasr Al Ainy,Garden City, Cairo, Egypt',
            'created_by' => 2,
        ];

        $rows[] = [
            'field_type' => 'url',
            'field_class' => '',
            'type' => 'Social Links',
            'field' => 'facebook',
            'label:en' => 'Facebook',
            'label:ar' => 'Facebook',
            'value:en' => 'https://www.facebook.com',
            'value:ar' => 'https://www.facebook.com',
            'created_by' => 2,
        ];

        $rows[] = [
            'field_type' => 'url',
            'field_class' => '',
            'type' => 'Social Links',
            'field' => 'twitter',
            'label:en' => 'Twitter',
            'label:ar' => 'Twitter',
            'value:en' => 'https://www.twitter.com',
            'value:ar' => 'https://www.twitter.com',
            'created_by' => 2,
        ];

        $rows[] = [
            'field_type' => 'url',
            'field_class' => '',
            'type' => 'Social Links',
            'field' => 'instagram',
            'label:en' => 'Instagram',
            'label:ar' => 'Instagram',
            'value:en' => 'https://www.instagram.com',
            'value:ar' => 'https://www.instagram.com',
            'created_by' => 2,
        ];

        $rows[] = [
            'field_type' => 'url',
            'field_class' => '',
            'type' => 'Social Links',
            'field' => 'youtube',
            'label:en' => 'Youtube',
            'label:ar' => 'Youtube',
            'value:en' => 'https://www.youtube.com',
            'value:ar' => 'https://www.youtube.com',
            'created_by' => 2,
        ];

        $rows[] = [
            'field_type' => 'url',
            'field_class' => '',
            'type' => 'Social Links',
            'field' => 'linkedin',
            'label:en' => 'LinkedIn',
            'label:ar' => 'LinkedIn',
            'value:en' => 'https://www.linkedin.com',
            'value:ar' => 'https://www.linkedin.com',
            'created_by' => 2,
        ];


        foreach ($rows as $row) {
            \App\Modules\Configs\Configs::create($row);
        }
    }
}

