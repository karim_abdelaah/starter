<?php

namespace App\Modules\Configs;

use App\Modules\BaseApp\BaseModel;
use App\Modules\BaseApp\Traits\CreatedBy;
use Astrotomic\Translatable\Translatable;

class Configs extends BaseModel {
    use CreatedBy, Translatable;

    protected $table = "configs";

    protected $fillable = [
        'field_type',
        'field_class',
        'type',
        'field',
        'created_by'
    ];

    protected $translationForeignKey = "config_filed_id";
    protected $translatedAttributes = [
        'label',
        'value'
    ];

    public $rules = [
        'type' => 'required',
        'field' => 'required',
    ];

}
