<?php
return [
    'name' => 'Config Module',
    'description' => 'Application Config Modules',
    'status' => true,
    'autoload' => [
        'Helpers/functions.php'
    ],
    'middleware' => [

    ]
];
