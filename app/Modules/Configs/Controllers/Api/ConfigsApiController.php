<?php

namespace App\Modules\Configs\Controllers\Api;

use App\Modules\Configs\Configs;
use App\Modules\Configs\Requests\ConfigRequest;
use App\Modules\BaseApp\Controllers\BaseApiController;
use App\Modules\BaseApp\Enums\ResourceTypes;
use App\Modules\Configs\Requests\Api\ContactInfoApiRequest;
use App\Modules\Configs\Transformers\ContactInfoMobileTransformer;
use App\Modules\Configs\Transformers\ContactInfoResource;
use App\Modules\Configs\Transformers\ContactInfoTransformer;
use Illuminate\Support\Str;
use Exception;
use Swis\JsonApi\Client\Interfaces\ParserInterface;

class ConfigsApiController extends BaseApiController {

    private $model;

    public function __construct(ParserInterface $parserInterface , Configs $config)
    {
        $this->parserInterface = $parserInterface;
        $this->model = $config;
    }

    public function getContactInfo(ContactInfoApiRequest $request)
    {
        $info = $this->model->whereIn('type',["Contact Information","Social Links"])->get();

        $include = '';
        if($request->include){
            $include = $request->include;
        }
        $meta = [];

        return $this->transformDataModInclude($info, $include,new ContactInfoTransformer(), ResourceTypes::CONTACTINFO, $meta);
    }

    public function getMobileContactInfo(ContactInfoApiRequest $request)
    {
        $info = $this->model->whereIn('type',["Contact Information","Social Links"])->get();
//        $info =   Configs::with('translations')->listsTranslations('value')
//            ->whereIn('type', ["Contact Information","Social Links"])
//            ->select(['field','value'])
//            ->pluck('value','field')->toArray();

        $data = new \stdClass();
        foreach ($info as $item){
            $data->{$item->field} = $item->translateOrDefault(lang())->value ?? '';
        }

        $include = '';
        if($request->include){
            $include = $request->include;
        }
        $meta = [];
        return $this->transformDataModInclude($data, $include,new ContactInfoMobileTransformer(), ResourceTypes::CONTACTINFO, $meta);
    }

}
