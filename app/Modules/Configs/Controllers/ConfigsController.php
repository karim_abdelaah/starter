<?php

namespace App\Modules\Configs\Controllers;

use App\Modules\Configs\Configs;
use App\Modules\Configs\Requests\ConfigRequest;
use App\Http\Controllers\Controller;
use App\Modules\BaseApp\Controllers\BaseApiController;
use App\Modules\BaseApp\Enums\ResourceTypes;
use App\Modules\Configs\Requests\Api\ContactInfoApiRequest;
use App\Modules\Configs\Transformers\ContactInfoResource;
use App\Modules\Configs\Transformers\ContactInfoTransformer;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use File;

class ConfigsController extends BaseApiController {

    public $model;
    public $module, $views;

    public function __construct(Configs $model) {
        $this->middleware(['isSuperAdmin'])->except("getContactInfo");
        $this->module = 'configs';
        $this->views = 'Configs::configs';
        $this->title = trans('app.Configs');
        $this->model = $model;
    }

    public function getEdit() {
        $data['module'] = $this->module;
        $data['views'] = $this->views;
        $data['page_title'] = trans('app.Create') . " " . $this->title;
        $data['breadcrumb'] = [$this->title => $this->module . '/edit'];
        $data['rows'] = $this->model->get()->groupBy('type');
        $data['to_be_translated'] = ['application_name' , 'welcome' , 'contact_title' , 'contact_description' ,'head_office'];
        return view($this->views . '.edit', $data);
    }

    public function postEdit(ConfigRequest $request) {
//         dd($request->all());
        $home_page_fields = ['Home Page Information','Home Page Numbers'];
        $rows = $this->model->whereNotIn('type' ,$home_page_fields)->get();
        $to_be_translated = ['application_name' , 'welcome' , 'contact_title' , 'contact_description' ,'head_office'];

        if ($rows) {
            foreach ($rows as $row) {
                
                if ($row->field_type == 'file') {
                    $field = 'input_' . $row->id;
                    if ($request->hasFile($field)) {
                        $uploadPath = 'uploads';
                        $image = $request->file($field);
                        $fileName = strtolower(Str::random(10)) . time() . '.' . $image->getClientOriginalExtension();
                        $request->file($field)->move($uploadPath, $fileName);
                        $filePath = $uploadPath . '/' . $fileName;
                        if ($filePath) {
                            $imageSizes = ['small' => 'resize,200x200', 'large' => 'resize,400x300'];
                            foreach ($imageSizes as $key => $value) {
                                $value = explode(',', $value);
                                $type = $value[0];
                                $dimensions = explode('x', $value[1]);
                                if (!File::exists($uploadPath . '/' . $key)) {
                                    @mkdir($uploadPath . '/' . $key);
                                    @chmod($uploadPath . '/' . $key, 0777);
                                }
                                $thumbPath = $uploadPath . '/' . $key . '/' . $fileName;
                                $image = Image::make($filePath);
                                if ($type == 'crop') {
                                    $image->fit($dimensions[0], $dimensions[1]);
                                }
                                else {
                                    $image->resize($dimensions[0], $dimensions[1], function ($constraint) {
                                        $constraint->aspectRatio();
                                    });
                                }
                                $image->save($thumbPath);
                            }
                            @unlink($filePath);
                        }
                        $row->value = $fileName;
                        $row->save();
                    }
                }
                elseif (in_array( $row->field, $to_be_translated)){
                    foreach (langs() as $lang){
                        $row->update(["value:".$lang=>$request->{$row->field.":".$lang}]);
                    }
                }
                else {
                    $row->update(["value:ar"=>$request->{$row->field.":ar"}]);
                }
            }
        }
        \Cache::forget('configs');
        flash(trans('app.Update successfully'))->success();
        return back();
    }

}
