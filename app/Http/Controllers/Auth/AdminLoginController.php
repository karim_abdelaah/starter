<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminLoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:adminguard', ['except' => ['logout']]);
    }
    public function getloginpage()
    {
        return view('auth.admin.adminlogin');
    }
    public function login(Request $request)
    {
        // return $request->all();
        // Validate the form data
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);
        // Attempt to log the user in
        if (Auth::guard('adminguard')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            // if successful, then redirect to their intended location
            return redirect()->intended(route('admin.dashboard'));
        }
        // return ('sasas');
        // if unsuccessful, then redirect back to the login with the form data
        // session()->flash('msg', "Wrong Password");
        // session()->flash('type', 'error');
        return redirect()->back()->withInput($request->only('email', 'remember'))->with('msg', "Wrong Password")->with('type', 'error');
    }

    public function logout()
    {
        Auth::guard('adminguard')->logout();
        return redirect(route('admin.dashboard'));
    }
}
